# tests/gpio_test.py
#
# Run tests with 'python -m pytest' in main directory
#
# Copyright (c) 2022 Spring City Solutions LLC and other contributors
# Licensed under the Apache License, Version 2.0

import pytest

import gpio

def test_parse_args_default():
    parser = gpio.parse_args([])
    assert gpio.register_data['pin'] == 6

def test_parse_args_pin():
    parser = gpio.parse_args(['--pin', 'P22'])
    assert gpio.register_data['pin'] == 6
    parser = gpio.parse_args(['--pin', 'P23'])
    assert gpio.register_data['pin'] == 13
    parser = gpio.parse_args(['--pin', 'P24'])
    assert gpio.register_data['pin'] == 19
    parser = gpio.parse_args(['--pin', 'P25'])
    assert gpio.register_data['pin'] == 26
