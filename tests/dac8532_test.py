# tests/dac8532_test.py
#
# Run tests with 'python -m pytest' in main directory
#
# Copyright (c) 2022 Spring City Solutions LLC and other contributors
# Licensed under the Apache License, Version 2.0

import pytest

import dac8532

def test_parse_args_default():
    parser = dac8532.parse_args([])
    assert dac8532.register_data['control'] == 0x30

def test_parse_args_port():
    parser = dac8532.parse_args(['--port', 'A'])
    assert dac8532.register_data['control'] == 0x30
    parser = dac8532.parse_args(['--port', 'B'])
    assert dac8532.register_data['control'] == 0x34
